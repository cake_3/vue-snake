export const gameItems = [
  {
    id: 'Snake',
    type: 'snake',
    cords: [],
    length: 1
  },
  {
    id: 'Rat',
    type: 'food',
    length: 1,
    cords: []
  }
]
